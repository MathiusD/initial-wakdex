from flask import Flask, redirect, render_template, request, make_response
from config.settings import LOG, DB, HOST
from own import own_sqlite
from http import HTTPStatus
from werkzeug.exceptions import HTTPException
import logging, time
from flask_assets import Bundle, Environment

logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', filename='logs/%s.log'%(str(time.time())), level=LOG)

logging.info("Verification Database")
if (own_sqlite.bdd_exist(DB) != True):
    logging.info("Creating Database")
    own_sqlite.bdd_create(DB)

logging.info("Connecting Database")
bdd = own_sqlite.connect(DB)

app = Flask(__name__)
assets = Environment(app)
cssBase = Bundle("../assets/scss/style.scss", filters="libsass", output="gen/style.css")
assets.register("scss_base", cssBase)
cassMob = Bundle("../assets/scss/mob.scss", filters="libsass", output="gen/mob.css")
assets.register("scss_mob", cassMob)
cssFamily = Bundle("../assets/scss/family.scss", filters="libsass", output="gen/family.css")
assets.register("scss_family", cssFamily)

@app.errorhandler(HTTPException)
def page_not_found(error:HTTPException):
    response = make_response(render_template('http_error.jinja', description=error.description, httpCode=error.code, httpError=error.name))
    response.headers["X-Robots-Tag"] = "noindex"
    return response, error.code

@app.route('/')
def index():
    return render_template("base.jinja")

@app.route("/family/<id>")
def family(id:int):
    bouchtrou = {
        "name":"Bouchtrou l'esseulé",
        "id":1,
        "type":"Archimonstre",
        "asset":"/static/img/bouchtrou.png",
        "lvl":{
            "max":28,
            "min":23
        },
        "roles":[
            {
                "name":"Dégâts",
                "asset":"/static/img/dpt.png"
            },
            {
                "name":"Tank",
                "asset":"/static/img/tank.png"
            },
            {
                "name":"Soutien",
                "asset":"/static/img/support.png"
            }
        ]
    }
    family = {
        "id":1,
        "name":"Bouftou",
        "description":"Des Bouftous en tout genre",
        "locations":[
            "Plaines d'Astrub",
            "Tainela"
        ],
        "lvl":"Grossière (11-20)",
        "donjons":[
            "Paturage des Bouftous"
        ],
        "mobs":[
            bouchtrou
        ],
        "img":"/static/img/bouftou.png"
    }
    familys = [
        {
            "name":"Chuchoteurs",
            "id":2
        },
        {
            "name":"Pichon",
            "id":2
        },
        {
            "name":"Karapatte",
            "id":666
        }
    ]
    return render_template("family.jinja", family=family, familys=familys)

@app.route('/mob/<id>')
def mob(id:int):
    mob = {
        "location":[
            "Plaines d'Astrub",
            "Tainela"
        ],
        "type":"Archimonstre",
        "description":"Archimonstre pour faire bouche-trou",
        "name":"Bouchtrou l'esseulé",
        "img":"/static/img/bouchtrou-portrait.png",
        "family":[
            {
                "name":"Bouftou",
                "id":1
            }
        ],
        "pv":{
            "max":1000,
            "min":750
        },
        "lvl":{
            "max":28,
            "min":23
        },
        "pa":10,
        "pm":4,
        "pw":0,
        "since":{
            "name":"Nouveaux Jours à Astrub",
            "ref":"1.48"
        },
        "roles":[
            {
                "name":"Dégâts",
                "asset":"/static/img/dpt.png"
            },
            {
                "name":"Tank",
                "asset":"/static/img/tank.png"
            },
            {
                "name":"Soutien",
                "asset":"/static/img/support.png"
            }
        ],
        "capturable":False,
        "masteries":{
            "fire":{
                "max":430,
                "min":330
            },
            "water":{
                "max":0,
                "min":0
            },
            "earth":{
                "max":430,
                "min":330
            },
            "air":{
                "max":0,
                "min":0
            }
        },
        "resistances":{
            "fire":{
                "max":300,
                "min":300
            },
            "water":{
                "max":200,
                "min":200
            },
            "earth":{
                "max":400,
                "min":400
            },
            "air":{
                "max":100,
                "min":100
            }
        },
        "spells":[
            {
                "name":"Dernier Rempart",
                "po":{
                    "min":0,
                    "max":0
                },
                "cost":1,
                "target":"caster",
                "type":"mono",
                "desc":[
                    "Applique `E-0`(1 tour) à tout les alliés (`A-0``A-1`)."
                ],
                "effect":[
                    {
                        "name":"Dernier rempart",
                        "desc":[
                            "100 résistances élementaires"
                        ],
                        "leveled":False,
                        "ico":None
                    }
                ],
                "condition":[
                    "4 tours de recharge",
                    "PV courants : `A-2` < 50%"
                ],
                "gif":"/static/gif/bouchtrou-rempart.gif",
                "ico":"/static/ico/dernier-rempart.png",
                "assets":[
                    "/static/img/ally.png",
                    "/static/img/caster.png",
                    "/static/img/pv.png"
                ]
            },
            {
                "name":"Charge Royale",
                "po":{
                    "min":2,
                    "max":3
                },
                "cost":2,
                "target":"enemy",
                "type":"line",
                "desc":[
                    "Se rapproche de 2 cases",
                    "Dommage `A-0` : 10",
                    "`A-1` `E-0` (1 tour)"
                ],
                "effect":[
                    {
                        "name":"Sonné",
                        "desc":[
                            "Passe son tour",
                            "Ne peut plus tacler"
                        ],
                        "leveled":False,
                        "ico":"/static/img/stun.png"
                    }
                ],
                "condition":[
                    "2 tours de recharge"
                ],
                "gif":"/static/gif/bouchtrou-charge.gif",
                "ico":"/static/ico/charge.png",
                "assets": [
                    "/static/img/spellEarth.png",
                    "/static/img/enemy.png"
                ]
            },
            {
                "name":"Morsure Royale",
                "po":{
                    "min":1,
                    "max":1
                },
                "cost":3,
                "target":None,
                "type":"mono",
                "desc":[
                    "Dommage `A-0` : 11"
                ],
                "effect":None,
                "condition":[
                    "1 utilisation par cible.",
                    "2 utilisations par tour."
                ],
                "gif":"/static/gif/bouchtrou-morsure.gif",
                "ico":"/static/ico/morsure.png",
                "assets": [
                    "/static/img/spellEarth.png"
                ]
            },
            {
                "name":"Foudre Royale",
                "cost":4,
                "po":{
                    "min":1,
                    "max":4
                },
                "target":None,
                "type":"area",
                "desc":[
                    "Dommage `A-0` : 22 `A-1` (3)"
                ],
                "effect":None,
                "condition":[
                    "1 utilisation par cible.",
                    "2 utilisations par tour."
                ],
                "gif":"/static/gif/bouchtrou-foudre.gif",
                "ico":"/static/ico/foudre-royale.png",
                "assets": [
                    "/static/img/spellFire.png",
                    "/static/img/line.png"
                ]
            }
        ],
        "passives":[
            {
                "name":"Bouf'Rage",
                "desc":[
                    "`A-1``E-0` (Niv. 30)."
                ],
                "effect":[
                    {
                        "name":"Bouf'Rage",
                        "desc":[
                            "`C-VALUE-lvl`% Dommages Infligés"
                        ],
                        "defaultLvl":30,
                        "minLvl":1,
                        "maxLvl":30,
                        "leveled":True,
                        "ico":"/static/ico/bouf'rage.png"
                    }
                ],
                "leveled":False,
                "ico":"/static/ico/bouf'rage.png",
                "condition":[
                    "PV courants : `A-0` < 50%"
                ],
                "assets": [
                    "/static/img/pv.png",
                    "/static/img/caster.png"
                ]
            }
        ],
        "stats":[
            {
                "name":"critical",
                "desc":"Coup Critique",
                "min":25,
                "max":25,
                "suffix":"%"
            },
            {
                "name":"block",
                "desc":"Parade",
                "min":25,
                "max":25,
                "suffix":"%"
            },
            {
                "name":"initiative",
                "desc":"Initiative",
                "min":24,
                "max":39,
                "suffix":None
            },
            {
                "name":"dodge",
                "desc":"Esquive",
                "min":18,
                "max":33,
                "suffix":None
            },
            {
                "name":"lock",
                "desc":"Tacle",
                "min":30,
                "max":45,
                "suffix":None
            },
            {
                "name":"willpower",
                "desc":"Volonté",
                "min":0,
                "max":0,
                "suffix":None
            }
        ],
        "timeline":[
            {
                "name":"Dégâts Finaux",
                "desc":"100% Dommages Infligés",
                "img":"/static/img/bouchtrou-df.png"
            }
        ],
        "borderCase": [
            {
                "name":"Boue",
                "img":"/static/img/stab.png",
                "desc":[
                    "-2 `A-0`"
                ],
                "effect":[],
                "assets":[
                    "/static/img/pm.png"
                ]
            }
        ]
    }
    return render_template("mob.jinja", mob=mob, pow=pow, int=int, eval=eval, str=str)