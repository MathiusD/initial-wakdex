#!/bin/bash
# Setup This Repo
echo "Setup Repo";
FILE=./config/settings.py
if ! [ -f "$FILE" ]; then
    cp ./config/settings.sample.py $FILE;
fi
DIR=./logs
if ! [ -d "$DIR" ]; then
    mkdir $DIR;
fi
DIR=./data
if ! [ -d "$DIR" ]; then
    mkdir $DIR;
fi
git clone https://gitlab.com/MathiusD/owninstall;
git clone https://github.com/StartBootstrap/startbootstrap-freelancer assets/scss/freelancer --depth 1
git clone https://github.com/twbs/bootstrap assets/scss/freelancer/src/scss/bootstrap --depth 1
cp owninstall/install.py own_install.py;
python3 build.py;
rm -rf owninstall;